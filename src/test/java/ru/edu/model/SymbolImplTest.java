package ru.edu.model;

import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Time;
import java.time.Instant;

import static org.junit.Assert.*;

public class SymbolImplTest {

    @Test
    public void symbolTest() {
        SymbolImpl symbol = new SymbolImpl();

        Instant time = Instant.now();
        symbol.setSymbol("symbol");
        symbol.setTimestamp(time);
        symbol.setPrice(BigDecimal.valueOf(10));

        assertEquals("symbol", symbol.getSymbol());
        assertEquals(time, symbol.getTimeStamp());
        assertEquals(BigDecimal.valueOf(10), symbol.getPrice());
        assertEquals("SymbolImpl{symbol='symbol', price=10, timestamp=" + time.toString() + "}", symbol.toString());

    }


}