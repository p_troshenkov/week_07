package ru.edu;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;

import java.time.Instant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SymbolPriceServiceTest {

    public static final String SYMBOL_NAME = "BTCUSDT";

    private SymbolPriceService service;

    private CachedSymbolPriceService cachedService;

    private Symbol price;

    @Test
    public void getPrice() throws InterruptedException {

        /**
         * примерный тест проверки работы кэша, делаем 2 вызова и смотрим что данные были получены в одно время
         * после ожидания таймаута жизни данных в кэше делаем повторный запрос и проверяем, что данные имеют метку времени,
         * полученную после сброса.
         */
        service = new BinanceSymbolPriceService();
        cachedService = new CachedSymbolPriceService(service);

        Symbol price = cachedService.getPrice(SYMBOL_NAME);
        Symbol priceAgain = cachedService.getPrice(SYMBOL_NAME);


        assertEquals(price.getTimeStamp(), priceAgain.getTimeStamp());

        System.out.println("Ждем сброса кэша");
        Thread.sleep(11_000L);

        Symbol priceNew = cachedService.getPrice(SYMBOL_NAME);
        assertTrue(price.getTimeStamp().isBefore(priceNew.getTimeStamp()));
    }
}