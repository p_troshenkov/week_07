package ru.edu;

import org.junit.Test;
import ru.edu.model.Symbol;

import static org.junit.Assert.*;

public class BinanceSymbolPriceServiceTest {


    @Test
    public void getPrice() {
        SymbolPriceService service = new BinanceSymbolPriceService();
        Symbol price = service.getPrice("BTCUSDT");
        assertNotNull(price);
    }


    @Test (expected = IllegalStateException.class)
    public void getPriceNull() {
        SymbolPriceService service = new BinanceSymbolPriceService();
        Symbol price = service.getPrice(null);

    }


}