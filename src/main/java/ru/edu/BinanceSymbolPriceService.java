package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;


public class BinanceSymbolPriceService implements SymbolPriceService {

    /**
     * LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(BinanceSymbolPriceService.class);
    /**
     * RestTemplate.
     */
    private RestTemplate restTemplate = new RestTemplate();
    /**
     * URL.
     */
    public static final String URL =
            "https://api.binance.com/api/v3/ticker/price?symbol=";
    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут обновляться
     * не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        ResponseEntity<SymbolImpl> response = null;
        try {
            response = restTemplate.getForEntity(
                    URL + symbolName,
                    SymbolImpl.class
            );
        } catch (
                HttpClientErrorException
              | HttpServerErrorException
              | NullPointerException e
        ) {
            throw new IllegalStateException(e);
        }

        if (response.getStatusCode() != HttpStatus.OK) {
            LOGGER.error(
                    "НЕ получили ответ. Код ответа: {}",
                    response.getStatusCode()
            );
            return null;
        }
        SymbolImpl symbol = response.getBody();
        LOGGER.debug("Получили ответ для {}: {}", symbolName, symbol);
        return symbol;
    }
}
