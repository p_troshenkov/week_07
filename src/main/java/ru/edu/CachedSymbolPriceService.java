package ru.edu;

import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CachedSymbolPriceService implements SymbolPriceService {
    /**
     * SymbolPriceService.
     */
    private final SymbolPriceService delegate;

    /**
     * Cache.
     */
    private final Map<String, Symbol> cache = Collections
            .synchronizedMap(new HashMap<>());
    /**
     * Update time.
     */
    public static final int CACHE_TIME_UPDATE = 10;


    /**
     * Delegate.
     * @param service
     */
    public CachedSymbolPriceService(final SymbolPriceService service) {
        delegate = service;
    }


    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут обновляться
     * не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    @Override
    public Symbol getPrice(final String symbolName) {

        synchronized (symbolName) {

            if (!cache.containsKey(symbolName) || Instant.now()
                    .minus(CACHE_TIME_UPDATE, ChronoUnit.SECONDS)
                    .isAfter(cache.get(symbolName).getTimeStamp())
            ) {
                cache.put(symbolName, delegate.getPrice(symbolName));
            }
            return cache.get(symbolName);
        }
    }
}
