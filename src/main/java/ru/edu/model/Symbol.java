package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {

    /**
     * getSymbol.
     * @return - String
     */
    String getSymbol();

    /**
     * getPrice.
     * @return - BigDecimal
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return - Instant
     */
    Instant getTimeStamp();
}
