package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

public class SymbolImpl implements Symbol {

    /**
     * Name of symbol.
     */
    private String symbol;

    /**
     * Price.
     */
    private BigDecimal price;

    /**
     * Data timestamp.
     */
    private Instant timestamp = Instant.now();

    /**
     * getSymbol.
     * @return - String
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * setSymbol.
     * @param sSymbol
     */
    public void setSymbol(final String sSymbol) {
        symbol = sSymbol;
    }

    /**
     * getPrice.
     * @return - BigDecimal
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * setPrice.
     * @param sPrice
     */
    public void setPrice(final BigDecimal sPrice) {
        price = sPrice;
    }

    /**
     * Время получения данных.
     *
     * @return
     */
    @Override
    public Instant getTimeStamp() {
        return timestamp;
    }

    /**
     * setTimestamp.
     * @param sTimestamp
     */
    public void setTimestamp(final Instant sTimestamp) {
        timestamp = sTimestamp;
    }

    /**
     * toString.
     *
     * @return String
     */
    @Override
    public String toString() {
        return "SymbolImpl{"
                + "symbol='" + symbol + '\''
                + ", price=" + price
                + ", timestamp=" + timestamp
                + '}';
    }
}
